using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CotMServer
{
    class Program
    {
        static Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

        public static Stopwatch TimeNow = new Stopwatch();

        public static List<Client> Clients = new List<Client>();
        static BinaryFormatter formatter = new BinaryFormatter();

        static DataClassesDataContext db;

        static void Main(string[] args)
        {
            Console.Title = "Server";
            db = new DataClassesDataContext();

            socket.Bind(new IPEndPoint(IPAddress.Any, 2048));
            socket.Listen(0);

            socket.BeginAccept(AcceptCallback, null);

            TimeNow.Start();

            Console.ReadLine();
        }

        static void AcceptCallback(IAsyncResult ar)
        {
            Client client = new Client(socket.EndAccept(ar));

            Clients.Add(client);
            Console.WriteLine("\nNew Connection");

            socket.BeginAccept(AcceptCallback, null);

            Thread thread = new Thread(HandleClient);
            thread.Start(client);
        }

        static void HandleClient(object o)
        {
            Client client = (Client)o;

            while (true)
            {
                try
                {
                    var (type, data) = (Tuple<string, string[]>)formatter.Deserialize(client.Stream);

                    var responseMessage = "";

                    switch (type)
                    {
                        case "LoginRegister":
                            {
                                var (login, password) = (data[0], data[1]);

                                var player = db.Players.FirstOrDefault(p => p.Name == login);
                                if (player == null)
                                {
                                    //Create New Player
                                    player = new Player() { Name = login, Password = password, Role = db.Roles.First(), Score = new Score() { MaxFloor = 0 } };
                                    db.Scores.InsertOnSubmit(player.Score);
                                    db.Players.InsertOnSubmit(player);

                                    db.SubmitChanges();

                                    responseMessage = "HEEEEEEY, YOU ARE REGISTERED!!!";

                                    Console.WriteLine($"{player.Name} registered");

                                    client.Player = player;
                                }
                                else
                                {
                                    if (player.Password == password)
                                    {
                                        Console.WriteLine($"{player.Name} logged in");
                                        responseMessage = "Successfully logged in";

                                        client.Player = player;
                                    }
                                    else
                                    {
                                        responseMessage = "Wrong password";
                                    }

                                }

                                break;
                            }
                        case "GameResults":
                            {
                                int floor, money; 
                                try
                                {
                                    (floor, money) = 
                                        (Convert.ToInt32(data[0]), Convert.ToInt32(data[1]));
                                }
                                catch (Exception)
                                {
                                    responseMessage = "Incorrect data";
                                    break;
                                }

                                responseMessage = "Ok, you got some rewards :)";

                                var player = client.Player;

                                var newAchivements = new List<Achivement>();

                                if (player != null)
                                {
                                    #region Give achivements
                                    if (!player.AchivementToPlayers.Any(x => x.Player == player && x.AchivementId == 1))
                                    {
                                        newAchivements.Add(db.Achivements.First(x => x.Id == 1));
                                    }
                                    if (player.Score.MaxFloor < 2 && floor > 1)
                                    {
                                        newAchivements.Add(db.Achivements.First(x => x.Id == 2));
                                    }

                                    if (newAchivements.Any())
                                    {
                                        responseMessage += $"\n\nNew Achivements:";
                                    }
                                    foreach (var achivement in newAchivements)
                                    {
                                        responseMessage += "\n" + achivement.Name + " - " + achivement.Description;
                                        db.AchivementToPlayers.InsertOnSubmit(new AchivementToPlayer() { Achivement = achivement, Player = player });
                                    }

                                    
                                    #endregion

                                    if (floor > player.Score.MaxFloor)
                                    {
                                        responseMessage += $"\n\nYour max floor is {floor} now!";
                                        player.Score.MaxFloor = floor;
                                    }

                                    var experience = floor * 10;
                                    var newExp = player.Experience + experience;
                                    responseMessage += $"\n\nExperience {player.Experience} + {experience} = {newExp}";
                                    player.Experience = newExp;

                                    player.Money = money;
                                    responseMessage += $"\n\nAnd you have {money} gold!";

                                    db.SubmitChanges();
                                }

                                break;
                            }
                        case "GetGold":
                            {
                                var player = client.Player;

                                if (player != null)
                                {
                                    responseMessage = player.Money.ToString();
                                }
                                break;
                            }
                        default:
                            break;
                    }

                    formatter.Serialize(client.Stream, responseMessage);
                }
                catch
                {
                    client.Socket.Shutdown(SocketShutdown.Both);
                    client.Socket.Disconnect(true);
                    Clients.Remove(client);
                    Console.WriteLine($"{(client.Player == null ? "Unregistered client" : client.Player.Name)} disconnected");
                    return;
                }
            }
        }
    }
}
