﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CotMServer
{
    class Client
    {
        public Player Player{get; set;}
        public Socket Socket { get; set; }
        public Stream Stream { get; private set; }

        public Client(Socket socket)
        {
            Socket = socket;
            Stream = new NetworkStream(Socket);
        }
    }
}
