﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;
    public float gravity;
    public float rotationSpeed;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;

    [SerializeField]
    private GameLogic _GameLogic;

    Vector3 warpPosition = Vector3.zero;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        // let the gameObject fall down
        //BackToTheStart();
    }

    void Update()
    {
        if (controller.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(/*Input.GetAxis("Horizontal")*/0.0f, 0.0f, Input.GetAxis("Vertical"));
            transform.Rotate(0, Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime, 0);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }
        controller.enabled = true;

        // Apply gravity
        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);


        if (transform.position.y < -5)
        {
            //BackToTheStart();
        }
    }
    private void LateUpdate()
    {
        if (warpPosition != Vector3.zero)
        {
            transform.position = warpPosition;
            warpPosition = Vector3.zero;
        }
    }

    void BackToTheStart()
    {
        gameObject.transform.position = new Vector3(0.2f, 5, 0.2f);
    }

}
