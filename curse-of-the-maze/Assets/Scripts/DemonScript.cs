﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonScript : MonoBehaviour
{
    private CharacterController controller;

    [SerializeField]
    private GameLogic _GameLogic;

    /*
    Idle 0-29
    Walking 30-64
    Running 65-83
    Jump 84-105
    Atack 106-126
    Fury 127-194
    Atack2 195-219
    Death 220-253
    */
    private Animation anim;
    private Vector3 oldPosition;

    private float speed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        controller.enabled = true;

        anim = gameObject.GetComponent<Animation>();

        oldPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!anim.isPlaying)
        {
            anim.Play("Walking");
        }

        if (!anim.IsPlaying("Atack") && !_GameLogic.Paused)
        {
            var moveDirection = new Vector3(0.0f, 0.0f, speed);


            moveDirection = transform.TransformDirection(moveDirection);

            // Move the controller
            controller.Move(moveDirection * Time.deltaTime);

            if (Vector3.Distance(oldPosition, transform.position) <= speed * 0.01)
            {
                transform.Rotate(0, 140, 0);
            }

            oldPosition = transform.position;
        }
    }

    private void GameOver()
    {
        _GameLogic.GameOver();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.name == "Player")
        {
            anim.Play("Atack");
            Invoke("GameOver", 0.5f);
        }
    }
}
