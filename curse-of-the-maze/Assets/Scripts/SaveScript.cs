﻿using Assets.Scripts;
using LiteDB;
using Nox7atra.Mazes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveScript : MonoBehaviour
{
    public bool IsLoadSave { get; private set; }
    private bool mazeGot = false;

    public Save Save { get; private set; } = new Save();

    [SerializeField]
    private PlayerScript _Player;
    [SerializeField]
    private PortalScript _Portal;
    [SerializeField]
    private DemonScript _Demon;

    [SerializeField]
    private GameLogic _GameLogic;

    public W4Maze MazeToSave;

    void Awake()
    {
        using (var db = new LiteDatabase(@"MazeSave.ldb"))
        {
            var saves = db.GetCollection<Save>("saves");
            if (saves.Count() > 0)
            {
                Save = saves.FindOne(Query.All(Query.Descending));

                IsLoadSave = true;
            }
            else
            {
                IsLoadSave = false;
            }
        }
    }

    public W4Maze GetMaze()
    {
        if (IsLoadSave && !mazeGot)
        {
            mazeGot = true;
            return Save.Maze;
        }
        else
        {
            return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!_GameLogic.Paused && Input.GetKeyDown(KeyCode.F5))
        {
            SaveGame();
        }
    }

    public void DropSave()
    {
        using (var db = new LiteDatabase(@"MazeSave.ldb"))
        {
            db.DropCollection("saves");
        }
    }

    void SaveGame()
    {
        DropSave();
        if (!_GameLogic.Paused)
        {
            Save.Maze = MazeToSave;

            Save.Username = ServerConnection.Username;

            Save.Floor = _GameLogic.CurrentFloor;
            Save.Gold = _GameLogic.Gold;

            Save.playerSave = new SavePosRot(_Player.transform);
            Save.portalSave = new SavePosRot(_Portal.transform);
            Save.demonSave = new SavePosRot(_Demon.transform);

            using (var db = new LiteDatabase(@"MazeSave.ldb"))
            {
                var saves = db.GetCollection<Save>("saves");
                saves.Insert(Save);
            }
        }
    }

    void OnAplicationQuit()
    {
        if (!_GameLogic.Paused)
        {
            SaveGame();
        }
        else
        {
            DropSave();
        }
    }
}
