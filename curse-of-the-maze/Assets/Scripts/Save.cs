﻿using Nox7atra.Mazes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Save
    {
        public int Id { get; set; }
        public W4Maze Maze { get; set; }

        public string Username { get; set; }

        public int Floor { get; set; }
        public int Gold { get; set; }

        public SavePosRot playerSave { get; set; }
        public SavePosRot portalSave { get; set; }
        public SavePosRot demonSave { get; set; }
    }

    public class SavePosRot
    {
        public float px { get; set; }
        public float py { get; set; }
        public float pz { get; set; }

        public float qx { get; set; }
        public float qy { get; set; }
        public float qz { get; set; }
        public float qw { get; set; }

        public SavePosRot()
        {

        }


        public SavePosRot(Transform transform)
        {
            px = transform.position.x;
            py = transform.position.y;
            pz = transform.position.z;

            qx = transform.rotation.x;
            qy = transform.rotation.y;
            qz = transform.rotation.z;
            qw = transform.rotation.w;
        }

        public Vector3 GetPosition()
        {
            return new Vector3(px, py, pz);
        }

        public Quaternion GetRotation()
        {
            return new Quaternion(qx, qy, qz, qw);
        }

        public void GetTransform(Transform transform)
        {
            transform.position = GetPosition();
            transform.rotation = GetRotation();
        }
    }
}
