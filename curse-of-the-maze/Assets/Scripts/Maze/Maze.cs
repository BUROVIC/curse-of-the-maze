﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Maze<T>
{
    public int Id { get; set; }
    public int ColumnCount { get; set; }
    public int RowCount { get; set; }
    public List<T> _Cells { get; set; }
    public Maze(int columnCount, int rowCount)
    {
        ColumnCount = columnCount;
        RowCount = rowCount;
    }
    public Maze()
    {
    }
    public virtual T GetCell(int x, int z)
    {

        if (x < ColumnCount && z < RowCount)
        {
            return _Cells[x + z * ColumnCount];
        }
        else
        {
            Debug.Log("Index out of range: "
                + (x < ColumnCount ?
                z.ToString() + ">=" + RowCount.ToString() :
                x.ToString() + ">=" + ColumnCount.ToString()));
            return default(T);
        }
    }
}
