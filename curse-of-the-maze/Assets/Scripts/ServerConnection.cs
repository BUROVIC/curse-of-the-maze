﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public static class ServerConnection
{
    static Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
    static BinaryFormatter formatter = new BinaryFormatter();
    public static string Username;

    public static bool Connect(string host)
    {
        try
        {
            if (!socket.Connected)
            {
                socket.Connect(host, 2048);
            }
            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }

    public static Task<string> SendQuery(string type, string[] message = null)
    {
        Stream stream = new NetworkStream(socket);

        formatter.Serialize(stream, new Tuple<string, string[]>(type, message));

        return Task.Run(() => { return (string)formatter.Deserialize(stream); });
    }
}

