﻿using Nox7atra.Mazes;
using Nox7atra.Mazes.Generators;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    [SerializeField]
    private SaveScript _SaveScript;
    [SerializeField]
    private MazeVisualizer _MazeVisualizer;
    [SerializeField]
    private PlayerScript _Player;
    [SerializeField]
    private DemonScript _Demon;
    [SerializeField]
    private PortalScript _Portal;

    [SerializeField]
    private GameObject GameResultPanel;

    [SerializeField]
    private Text _TextFloor;
    [SerializeField]
    private Text _TextGold;
    public int Gold { get { return int.Parse(_TextGold.text); } set { _TextGold.text = value.ToString(); } }

    public int CurrentFloor { get; private set; }

    double roomOffset;

    public bool Paused { get; private set; }
    bool LoadFirstTime = true;

    // Start is called before the first frame update
    void Start()
    {
        _TextGold.text = ServerConnection.SendQuery("GetGold").Result;
        GameResultPanel.SetActive(false);
        CurrentFloor = 0;
        NextFloor();
    }

    public void GameOver()
    {
        _SaveScript.DropSave();

        togglePause();
        var gameResultTask = ServerConnection.SendQuery("GameResults", new string[] { CurrentFloor.ToString(), Gold.ToString() });
        GameResultPanel.SetActive(true);
        GameResultPanel.transform.Find("TextGO").GetComponent<Text>().text = gameResultTask.Result;

        //RestartGame();
    }
    public void RestartGame()
    {

        togglePause();
        Start();
    }

    public void NextFloor()
    {
        if (CurrentFloor != 0)
        {
            Gold += Random.Range(100, 200);
        }

        Destroy(GameObject.Find("Maze"));

        _MazeVisualizer.RefreshMaze();
        roomOffset = 0.5 - _MazeVisualizer._WallThikness / 2;

        if (LoadFirstTime && _SaveScript.IsLoadSave && _SaveScript.Save.Username == ServerConnection.Username)
        {
            CurrentFloor = _SaveScript.Save.Floor - 1;
            Gold = _SaveScript.Save.Gold;

            _Player.transform.position = _SaveScript.Save.playerSave.GetPosition();
            _Player.transform.rotation = _SaveScript.Save.playerSave.GetRotation();

            _Portal.transform.position = _SaveScript.Save.portalSave.GetPosition();
            _Portal.transform.rotation = _SaveScript.Save.portalSave.GetRotation();

            _Demon.transform.position = _SaveScript.Save.demonSave.GetPosition();
            _Demon.transform.rotation = _SaveScript.Save.demonSave.GetRotation();

            LoadFirstTime = false;
        }
        else
        {
            var playerRoom = GetRandomRoomCoordinate();
            Teleport(_Player.gameObject, playerRoom.x, playerRoom.y);

            Vector2 portalRoom;
            do
            {
                portalRoom = GetRandomRoomCoordinate();
            } while (Vector2.Distance(playerRoom, portalRoom) < 2);
            Teleport(_Portal.gameObject, portalRoom.x, portalRoom.y);

            Vector2 demonRoom;
            do
            {
                demonRoom = GetRandomRoomCoordinate();
            } while (Vector2.Distance(playerRoom, demonRoom) < 2);
            Teleport(_Demon.gameObject, demonRoom.x, demonRoom.y);
        }

        CurrentFloor++;
        
        _TextFloor.text = _TextFloor.text.Split(' ')[0] + " " + CurrentFloor;
    }

    Vector2 GetRandomRoomCoordinate()
    {
        return new Vector2((float)(Random.Range(0, _MazeVisualizer._MazeCellsX - 1) + roomOffset), (float)(Random.Range(0, _MazeVisualizer._MazeCellsX - 1) + roomOffset));
    }

    void Teleport(GameObject gameObject, float x, float z)
    {
        gameObject.transform.position = new Vector3(x, gameObject.transform.position.y, z);
    }

    // Update is called once per frame
    void Update()
    {
        if (Paused && Input.GetKeyDown(KeyCode.Return))
        {
            RestartGame();
        }

        //Suicide
        if (!Paused && Input.GetKeyDown(KeyCode.F10))
        {
            GameOver();
        }
    }

    void togglePause()
    {
        if (Paused)
        {
            Time.timeScale = 1.0f;
        }
        else
        {
            Time.timeScale = 0.0f;
        }
        Paused = !Paused;
    }
}
