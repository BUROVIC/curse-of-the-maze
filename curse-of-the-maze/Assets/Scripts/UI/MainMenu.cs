﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public bool SkipMenu;
    public GameObject InputLogin;
    public GameObject InputPassword;
    public GameObject InputIp;
    public GameObject TextError;
    // Start is called before the first frame update
    void Start()
    {
        InputIp.GetComponent<InputField>().text = "127.0.0.1";

        if (SkipMenu && Application.isEditor)
        {
            InputLogin.GetComponent<InputField>().text = "admin";
            InputPassword.GetComponent<InputField>().text = "admin";
            StartGame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        //Попытка подключения к хосту
        if (!ServerConnection.Connect(InputIp.GetComponent<InputField>().text))
        {
            TextError.GetComponent<Text>().text = "Connection error";
            return;
        }

        var logStatus = ServerConnection.SendQuery("LoginRegister", new string[]
            {
                InputLogin.GetComponent<InputField>().text,
                InputPassword.GetComponent<InputField>().text
            }).Result;

        ServerConnection.Username = InputLogin.GetComponent<InputField>().text;
        TextError.GetComponent<Text>().text = logStatus;

        if (logStatus != "Wrong password")
        {
            SceneManager.LoadScene("Game");
        }

    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
